var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var Manufacturer = require("../models/manufacturer");
var ShoeModel = require("../models/shoe_model");
var Place = require("../models/place");
var Color = require("../models/color");

var ShoeSchema = new Schema({
  model: {
    type: String,
    validate:{
      isAsync: true,
      validator: function(v, cb){
        ShoeModel.count({name: v}, function(err, docs){
          console.log(docs);
          var msg = v + " podany model nie istnieje w bazie!";
          cb(docs==1,msg);
        });
      },
    }
  },
  manufacturer: {
    type: String,
    validate:{
      isAsync: true,
      validator: function(v, cb){
        Manufacturer.count({name: v}, function(err, docs){
          var msg = v + " podany producent nie istenieje w bazie!";
          cb(docs==1, msg);
        })
      }
    }
  },
  color: {
    type: String,
    validate:{
      isAsync: true,
      validator: function(v, cb){
        Color.count({name: v}, function(err, docs){
          var msg = v + " podany kolor nie istenieje w bazie!";
          cb(docs==1, msg);
        })
      }
    }
  },
  size: String,
  state: {type: Number, default: 1},
  place: {
    type: String,
    validate:{
      isAsync: true,
      validator: function(v, cb){
        Place.count({name: v}, function(err, docs){
          var msg = v + " podane miejsce nie istnieje w bazie!";
          cb(docs==1, msg);
        })
      }
    }
  }
});
var Shoe = mongoose.model("shoe", ShoeSchema);
module.exports = Shoe;










