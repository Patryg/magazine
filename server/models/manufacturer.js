var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var ManufacturerSchema = new Schema({
    name: String,
    phone_number: String
});

var Manufacturer = mongoose.model("manufacturer", ManufacturerSchema);

module.exports = Manufacturer;