var mongoose = require("mongoose");
var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var ShoeModelSchema = new Schema({
    name: String,
    manufacturer: String
});
var ShoeModel = mongoose.model("shoemodel", ShoeModelSchema);

module.exports = ShoeModel;