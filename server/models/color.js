var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var ColorSchema = new Schema({
    name: String,
});
var Color = mongoose.model("color", ColorSchema);

module.exports = Color;