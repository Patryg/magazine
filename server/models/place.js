var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var PlaceSchema = new Schema({
    name: String,
    info: String
});
var Place = mongoose.model("place", PlaceSchema);

module.exports = Place;