const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')
var mongoose = require('mongoose');


const app = express();
app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(cors());

var Manufacturer = require("../models/manufacturer");
var ShoeModel = require("../models/shoe_model");
var Place = require("../models/place");
var Color = require("../models/color");
var Shoe = require("../models/shoe");

mongoose.connect('mongodb://localhost/warehouseapp');
var db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", function(callback){
  console.log("Connection Succeeded");
});

var addPlace = function(name, info, callback){
  Place.count({name: name}, function(err, obj){
    if(err){
      console.log(err);
    }
    if(obj === 0){
      var new_model = new Place({
        name: name,
        info: info
      });
     new_model.save().then(function(){
      callback(true);
     });
    }else{
      callback(false);
    }
  });
}
var addColor = function(name, callback){
  Color.count({name: name}, function(err, obj){
    if(err){
      console.log(err);
    }
    if(obj === 0){
      var new_model = new Color({
        name: name
      });
     new_model.save().then(function(){
      callback(true);
     });
    }else{
      callback(false);
    }
  });
}

var addShoe = function(model, manufacturer, color, size, place, amount, callback){
  ShoeModel.count({name: model, manufacturer: manufacturer}, (err, obj) =>{
    if(obj){
      var shoes = [];
      for(var i = 1; i<=amount; i++){
        var newShoe = new Shoe({
          model: model,
          manufacturer: manufacturer,
          color: color,
          size: size,
          place: place,
        });
        shoes.push(newShoe);
      }
      console.log("MODEL " + model + "\n" + "MANUCATURER " + manufacturer + "\n" + "COLOR " + color + "\n"
      + "SIZE " + size + "\n" + "PLACE " + place);
      Shoe.insertMany(shoes, function(error, docs){
        if(error){
          callback(false, error.message);
          // callback(false);
        }
        if(docs != null){
          callback(true, docs);
        }
      });
    }else{
      callback(false, "Podany producent nie posiada tego modelu");
    }
    
  })
  
}

var addModel = function(name, manufacturer, callback){
  Manufacturer.findOne({name: manufacturer},function(err, obj){
    if(err){
      console.log(err);
    }
    if(obj != null){
      var new_model = new ShoeModel({
        name: name,
        manufacturer: manufacturer
      });

      ShoeModel.count({name: name, manufacturers: manufacturer}, function(err, obj2){
        if(obj2 === 0){
          new_model.save().then(function(){
            callback(true);
          });
        }else{
          callback(false);
        };
      });
    }else{
      callback(false);
    }
  });


    
}

/* 
  FUNKCJA ADDMANUFACTURER PRZYJMUJE WARTOSCI I SPRAWDZA CZY PODANA NAZWA PRODUCENTA JUZ SIE NIE POWTARZA
*/
var addManufacturer = function(name, phone_number, callback){
  var new_manufacturer = new Manufacturer({
    name: name,
    phone_number: phone_number
  });
  var boolean = false;
  Manufacturer.count({name: name}, function(err, obj){
     if(obj === 0){
      new_manufacturer.save().then(function(){ 
        callback(true);
      });
    }else{
      callback(false);
    }
  });
}

var toNaturalKey = function(a, id, callback){
  a.findById(id, function(err, obj){
    callback(obj.name);
  });
}


app.post('/manufacturer', (req, res) => {
    var db = req.db;
    var name = req.body.name;
    var phone_number = req.body.phone_number;
    addManufacturer(name, phone_number, function(returnValue){
      if(returnValue){
        res.send({
          success: true,
          message: "Manufacturer added"
        })
      }else{
        res.send({
          success: true,
          message: "Manufacturer cannot be added"
        })
      }
    })

  })


app.post('/models', (req, res)=>{
  var db = req.db;
  var name = req.body.name;
  var manufacturer = req.body.manufacturer;

  addModel(name, manufacturer, function(returnValue){
    if(returnValue){
      res.send({
        success: true,
        message: "Model added"
      })
    }else{
      res.send({
        success: true,
        message: "Model cannot be added"
      })
    }
  });
});

app.post('/places', (req, res)=>{
  var db = req.db;
  var name = req.body.name;
  var info = req.body.info;
  addPlace(name, info, function(returnValue){
    if(returnValue){
      res.send({
        success: true,
        message: "Place added"
      })
    }else{
      res.send({
        success: true,
        message: "Place cannot be added"
      })
    }
  });
});

app.post('/colors', (req, res)=>{
  var db = req.db;
  var name = req.body.name;
  addColor(name, function(returnValue){
    if(returnValue){
      res.send({
        success: true,
        message: "Color added"
      })
    }else{
      res.send({
        success: true,
        message: "Color cannot be added"
      })
    }
  });
});

app.post('/shoe', (req, res)=>{
  var db = req.db;
  var model = req.body.model;
  var manufacturer = req.body.manufacturer;
  var color = req.body.color;
  var size = req.body.size;
  var place = req.body.place;
  var amount = req.body.amount;

  addShoe(model, manufacturer, color, size, place, amount, function(returnValue, msg){
    if(returnValue){
      res.send({
        success: true,
        message: "Shoe added",
        ids: msg
      })
    }else{
      res.send({
        success: false,
        message: msg
      })
    }
  });

});
app.get('/manufacturer', (req, res)=>{
  Manufacturer.find({}, function(err, obj){
    if(err){
      console.log()
    }
    res.send({
      manufacturers: obj
    })
    
  }).sort({_id: -1});
});
app.get('/shoe', (req, res) =>{
  Shoe.find({}, function(err, obj){
    if(err){
      console.log()
    }
    res.send({
      shoes: obj
    })
    
  }).sort({_id: -1});
});
app.get('/places', (req, res) =>{
  Place.find({}, function(err, obj){
    if(err){
      console.log()
    }
    res.send({
      places: obj
    })
    
  }).sort({_id: -1});
});
app.get('/colors', (req, res) =>{
  Color.find({}, function(err, obj){
    if(err){
      console.log()
    }
    res.send({
      colors: obj
    })
    
  }).sort({_id: -1});
});
app.get('/models', (req, res) =>{
  ShoeModel.find({}, function(err, obj){
    if(err){
      console.log()
    }
    res.send({
      models: obj
    })
    
  }).sort({_id: -1});
});

app.get('/models/:manu', (req, res)=>{
  var db = req.db;
  console.log(req.params.manu)
  ShoeModel.find({'manufacturer': req.params.manu}, function(err, obj){
    if(err){
      console.log(err);
    }
    res.send({
      models: obj
    })

  }).sort({_id: -1});
});
app.delete('/shoe/:id', (req, res)=>{
  var db = req.db;
  Shoe.remove({
    _id: req.params.id
  }, function(err, shoe){
    if(err)
      res.send(err);
    res.send({
      success: true
    })
  })
});







app.listen(process.env.PORT || 8081)
