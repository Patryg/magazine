import Vue from 'vue';
import Vuex from 'vuex';
import { METHODS } from 'http';

Vue.use(Vuex);


export const store = new Vuex.Store({
    state:{
        isLoaded: false,
        progress: {
            infoLoaded: false,
            shoeLoaded: false
          }
    }
});