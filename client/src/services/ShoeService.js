import Api from '@/services/Api'


export default {
  fetchShoes () {
    return Api().get('shoe');
  },
  fetchManufacturers(){
    return Api().get('manufacturer');
  },
  fetchModelsByManufacturer(manu){
    return Api().get('models/' + manu)
  },
  fetchModels(){
    return Api().get('models');
  },
  fetchPlaces(){
    return Api().get('places');
  },
  fetchColors(){
    return Api().get('colors');
  },
  deleteShoe(id){
    return Api().delete('shoe/' + id).then(() =>{
      return true;
    });
  },
  addShoe(params){
    return Api().post('shoe', params).then((response)=>{
      return response;
    });
  },
  addManu(params){
    return Api().post('manufacturer', params).then((response)=>{
      return response;
    })
  },
  addModel(params){
    return Api().post('models', params).then((response)=>{
      return response;
    })
  },
  addColor(params){
    return Api().post('colors', params).then((response)=>{
      return response;
    })
  },
  addPlace(params){
    return Api().post('places', params).then((response)=>{
      return response;
    })
  }
}