import Vue from 'vue'
import Router from 'vue-router'
import mainindex from '@/components/mainindex'

Vue.use(Router)
export default new Router({
  routes: [
    {
      path: '/',
      name: 'mainindex',
      component: mainindex
    }
  ]
})
